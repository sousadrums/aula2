
$(document).ready(function(){
	var base_url="http://localhost/aula2/";
	var pupitre= $('.pupitre');
	pupitre.draggable({
  		containment: ".sala",
  		snap: true,
  		snapTolerance: 10,
		drag: function( event, ui ) {
			var snapTolerance = $(this).draggable('option', 'snapTolerance');
			var topRemainder = ui.position.top % 1;
			var leftRemainder = ui.position.left % 1;
	
			if (topRemainder <= snapTolerance) {
				ui.position.top = ui.position.top - topRemainder;
			}
	
			if (leftRemainder <= snapTolerance) {
				ui.position.left = ui.position.left - leftRemainder;
			}
		},
  		stop: function(e, ui){
  			var id= $(this).attr('id'),
			x= ui.position.left,
			y= ui.position.top,
			posicion={id: id, x: x, y: y};
			$.ajax({
				type: 'POST',
				url: location.hash+"ajax",
				data: posicion,
				success: function(response){
					
				},
				error: function(error){
					alert('error');
				}
			});
  		}
  		
	});
	$('.pop').popover({
		html:true,
		trigger:'hover'
	});	
});