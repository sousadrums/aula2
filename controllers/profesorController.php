<?php
require_once 'models/profesor.php';
require_once 'models/grupo.php';
require_once 'models/asignatura.php';

class profesorController{

	public function index(){
		require_once 'views/profesor/login.php';
	}
	

	public function register(){
		Utils::isAdmin();

		require_once 'views/profesor/navegacion.php';
		require_once 'views/profesor/registro.php';
	}

	public function loginV(){
		require_once 'views/profesor/login.php';
	}
	public function loged(){
		if (isset($_SESSION['admin'])){
			Utils::isAdmin();
			require_once 'views/profesor/navegacion.php';
		}elseif(isset($_SESSION['tutor'])){
			Utils::isTutor();
			$id_tutor=$_SESSION['identity']->id_profesor;
			$grupo=new grupo();
			$grupo->setId_tutor($id_tutor);
			$id=$grupo->grupoTutor();
			$id_grupo=$id->fetch_object()->id_grupo;
			$_SESSION['id_grupo']=$id_grupo;
			require_once 'views/profesor/navegacion.php';
		}elseif(isset($_SESSION['identity'])){
			Utils::isProfe();
			require_once 'views/profesor/navegacion.php';
		}else{
			require_once 'views/profesor/login.php';
			require_once 'views/profesor/navegacion.php';
		}
	}

	public function profesores(){
		Utils::isAdmin();
		$profesor=new Profesor();
		$profesores=$profesor->getAll();
		$gruposin=$profesor->grupoSin();
		$aulasin=$profesor->aulasin();
		
		require_once 'views/profesor/navegacion.php';
		require_once 'views/profesor/profesores.php';
	}

	public function tutores(){
		Utils::isAdmin();
		$profesor=new Profesor();
		$profesores=$profesor->getTutores();

		require_once 'views/profesor/navegacion.php';
		require_once 'views/profesor/tutores.php';
	}


	public function login(){
		if($_SERVER['REQUEST_METHOD']=='POST'){
			if(isset($_POST)){
				$token=$_POST['token'];
				if($_SESSION['token'] == $token){
					// Identificar al usuario
					// Consulta a la base de datos

					$profesor = new Profesor();
					$profesor->setUsuario( Utils::test_input($_POST['usuario']));
					$profesor->setContraseña(Utils::test_input($_POST['contraseña']));
					$identity = $profesor->login();
					

					if($identity && is_object($identity)){
						$_SESSION['identity'] = $identity;
						
						if($identity->rol == 'admin'){
							$_SESSION['admin'] = true;
						}elseif($identity->rol == 'tutor'){
							$_SESSION['tutor'] = true;
						}else{
							$_SESSION['profe']=true;
						}
					}else{
						$_SESSION['error_login'] = 'Identificación fallida !!';
					}
			
				}
				header("Location:".base_url."profesor/loged");
			}
		}
	}
	
	public function logout(){
		if(isset($_SESSION['identity'])){
			unset($_SESSION['identity']);
		}
		
		if(isset($_SESSION['admin'])){
			unset($_SESSION['admin']);
		}
		if(isset($_SESSION['tutor'])){
			unset($_SESSION['tutor']);
		}
		if(isset($_SESSION['profe'])){
			unset($_SESSION['profe']);
		}
		
		header("Location:".base_url."profesor/loginV");
	}

	public function save(){
		if (isset($_POST)) {
			
			$usuario = isset($_POST['usuario']) ?  $_POST['usuario'] : false;
			$contraseña = isset($_POST['contraseña']) ? $_POST['contraseña'] : false;
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;

			
			if($usuario && $contraseña && $nombre && $apellidos){
				$profesor= new Profesor();
				$profesor->setUsuario($usuario);
				$profesor->setContraseña($contraseña);
				$profesor->setNombre($nombre);
				$profesor->setApellidos($apellidos);
				
				$save= $profesor->save();

				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		header("Location:".base_url.'profesor/register');


	}

	public function edit(){
		if (isset($_SESSION)) {
			$usuario=$_SESSION['identity']->usuario;
			$nombre=$_SESSION['identity']->nombre;
			$apellidos=$_SESSION['identity']->apellidos;
	
			require_once 'views/profesor/navegacion.php';
			require_once 'views/profesor/eduser.php';

		}
	}


	public function borrar(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$profesor= new Profesor();
				$profesor->setId_profesor($id);
				$desasig=$profesor->quitarTutoria();
				$delete=$profesor->borrarProfe();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."profesor/profesores");
	}

	public function update(){
		if (isset($_POST)) {
			$profesor = new Profesor();
			$profesor->setId_profesor($_SESSION['identity']->id);
			$profesor->setUsuario($_POST['usuario']);
			$profesor->setNombre($_POST['nombre']);
			$profesor->setApellidos($_POST['apellido']);
			
			$save=$profesor->edit();

			session_unset();


			if ($save) {
				$_SESSION['profesor']="complete";
			}else{
				$_SESSION['profesor']="failed";
			}

		}else{
			$_SESSION['profesor']="failed";
		}
		header("Location:".base_url);
	}

	public function contraseña(){
	
		require_once 'views/profesor/navegacion.php';
		require_once 'views/profesor/ccontraseña.php';
	}
	public function ccontraseña(){
		if (isset($_POST)) {
			$old=$_POST['old'];
			$new1=$_POST['new1'];
			$new2=$_POST['new2'];
			$verify1 = password_verify($old, $_SESSION['identity']->contraseña);

			$verify2=false;
			if ($new1==$new2) {
				$verify2=true;
			}

			if (!$verify1) {
				$this->contraseña();
				echo '<div class="alert alert-danger">La contraseña no es correcta</div>';
				
			}elseif (!$verify2) {
				$this->contraseña();
				echo '<div class="alert alert-danger">Las contraseñas no coinciden</div>';

			}else{
				$profesor = new Profesor();
				$profesor->setId_profesor($_SESSION['identity']->id_profesor);
				$profesor->setContraseña($new2);
				$save=$profesor->updateContraseña();
				
				if ($save) {
					$_SESSION['uPassword']="complete";
					require_once 'views/profesor/cambiopassword.php';
				}else{
					$_SESSION['uPassword']="failed";
				}

			}

		}
		
		
	}

	public function asignarTutor(){
		Utils::isAdmin();
		require_once 'views/profesor/asignartutor.php';
		require_once 'views/profesor/navegacion.php';
	}

	public function asignarTutoria(){
		Utils::isAdmin();
		if(isset($_GET)){
			$id_profe=$_GET['id_profesor'];
			$profe=new Profesor();
			$profe->setId_profesor($id_profe);
			$profesor=$profe->getProfe();
			$gruposin=$profe->grupoSin();
			$aulasin=$profe->aulasin();
			
		}
		require_once 'views/profesor/navegacion.php';
		require_once 'views/profesor/asignartutor.php';

	}
	
	public function asignar(){
		Utils::isAdmin();
		
		if (isset($_POST)) {
			$id_grupo = isset($_POST['id_grupo']) ? $_POST['id_grupo'] : false;
			$id_aula=isset($_POST['id_aula']) ? $_POST['id_aula'] : false;
			$id_tutor=$_GET['id_profesor'];
			
			
			if($id_grupo && $id_aula && $id_tutor){
				$tutor=new Profesor();
				$tutor->setId_profesor($id_tutor);
				$tutor->aTutor();
				$grupot= new Grupo();
				$grupot->setId_grupo($id_grupo);
				$grupot->setId_tutor($id_tutor);
				$tutoria=new asignatura();
				$tutoria->setNombre("tutoria");
				$tutoria->setId_aula($id_aula);
				$tutoria->setId_profesor($id_tutor);
				$tutoria->setId_grupo($id_grupo);
				$save1=$grupot->tutorGrupo();
				$save2=$tutoria->tutoria();
				
			

				if ($save1 && $save2) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		require_once 'views/profesor/navegacion.php';
		header("Location:".base_url.'profesor/profesores');
	}

	public function quitarTutoria(){
		Utils::isAdmin();
		
		if (isset($_GET)) {
			$id_tutor=$_GET['id_profesor'];
			
			
			if($id_tutor){
				$tutor=new Profesor();
				$tutor->setId_profesor($id_tutor);
				$grupo=$tutor->quitarTutoria();
				$profe=$tutor->aProfe();
				$tutoria=new asignatura();
				$tutoria->setId_profesor($id_tutor);
				$tuto=$tutoria->eliminarTutoria();

				if ($grupo && $profe && $tuto) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		require_once 'views/profesor/navegacion.php';
		header("Location:".base_url.'profesor/profesores');
	}
	

}

