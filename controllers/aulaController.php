<?php 

require_once 'models/aula.php';
require_once 'models/grupo.php';
require_once 'models/alumno.php';
require_once 'models/relacion.php';
require_once 'models/posicion.php';
require_once 'models/asignatura.php';

class aulaController{

	public function index(){
		$aula=new aula();
		$aulas=$aula->aulas();
		require_once 'views/aula/aulas.php';
	}


	public function ver(){
		
		$aula=new aula();
		$aula->setId_aula($_GET['id_aula']);
		$clase=$aula->unAula();
		$tutoria=new asignatura();
		$tutoria->setId_aula($_GET['id_aula']);
		$id=$tutoria->grupoAula();
		$id_grupo=$id->fetch_object()->id_grupo;
		$grupo=new grupo();
		$grupo->setId_grupo($id_grupo);
		$id_tut=$grupo->idTutorGrupo();
		$id_tutor=$id_tut->fetch_object()->id_tutor;
		$alumno=new alumno();
		$alumno->setId_grupo($id_grupo);
		$alumnos=$alumno->alumnosClase();
		
		require_once 'views/profesor/navegacion.php';
		require_once 'views/aula/aula.php';
		
	}
	public function cargar(){
        $id_grupo=$_GET['id_grupo'];
        $id_tutor=$_SESSION['identity']->id_profesor;
        $guardado=$_POST['nombre'];
		$grupo=new grupo();
		$grupo->setId_tutor($id_tutor);
		$idauka=$grupo->aulaGrupo();
		$id_aula=$idauka->fetch_object()->id_aula;
		$aula=new aula();
		$aula->setId_aula($id_aula);
		$clase=$aula->unAula();
		$alumno=new alumno();
		$alumno->setId_grupo($id_grupo);
		$alumnos=$alumno->alumnosClaseg($guardado);
        
		require_once 'views/profesor/navegacion.php';
		require_once 'views/aula/aula.php';
    }
	public function verT($guardado="sin"){
		$id_tutor=$_SESSION['identity']->id_profesor;
		$id_grupo=$_SESSION['id_grupo'];
		if(isset($_POST['nombre'])){
			$guardado=$_POST['nombre'];
		}
		$grupo=new grupo();
		$grupo->setId_tutor($id_tutor);
		$idauka=$grupo->aulaGrupo();
		$id_aula=$idauka->fetch_object()->id_aula;
		$aula=new aula();
		$aula->setId_aula($id_aula);
		$clase=$aula->unAula();
		$tutoria=new asignatura();
		$tutoria->setId_aula($id_aula);
		$alumno=new alumno();
		$alumno->setId_grupo($id_grupo);
		$alumnos=$alumno->alumnosClaseg($guardado);
		$alumnoz=$alumno->alumnosClaseg($guardado);
		while ($alumno=$alumnoz->fetch_object()) {
			$posicion=new posicion();
			$id=$alumno->id_alumno;
			$x=$alumno->coordx;
			$y=$alumno->coordy;
			$posicion->setId_alumno($id);
			$posicion->setCoordx($x);
			$posicion->setCoordy($y);
			
			$posicion->mover();
		}

		require_once 'views/profesor/navegacion.php';
		require_once 'views/aula/aula.php';
		
	}

	public function ajax(){
		$posicion=new posicion();
		$posicion->setId_alumno($_POST['id']);
		$posicion->setCoordx($_POST['x']);
		$posicion->setCoordy($_POST['y']);
		$posicion->setGuardado("sin");
		$posicion->mover();
	}

	public function aulas(){
		Utils::isAdmin();
		$aula=new Aula();
		$aulas=$aula->aulas();
	
		require_once 'views/profesor/navegacion.php';
		require_once 'views/aula/aulas.php';
	}

	function crear(){
		Utils::isAdmin();
		require_once 'views/profesor/navegacion.php';
		require_once 'views/aula/crear.php';
	}

	public function save(){
		if (isset($_POST)) {
			
			$codigo = isset($_POST['codigo']) ? $_POST['codigo'] : false;
			$mesas = isset($_POST['mesas']) ? $_POST['mesas'] : false;


			if($codigo && $mesas){
				$aula= new Aula();
				$aula->setCodigo($codigo);
				$aula->setMesas($mesas);
				
				$save= $aula->save();
			
				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		
		header("Location:".base_url.'aula/aulas');
	}

	public function borrar(){
		Utils::isAdmin();
			if (isset($_GET['id_aula'])) {
				$id_aula=$_GET['id_aula'];
				$aula= new Aula();
				$aula->setId_aula($id_aula);
				$delete=$aula->borrarAula();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."aula/aulas");
	}




}

?>