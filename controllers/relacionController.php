<?php 

require_once 'models/relacion.php';
require_once 'models/alumno.php';

class relacionController{

	public function crear(){
		$alumno=new alumno();
		$alumno->setId_alumno($_SESSION['id']);
		$alumnox=$alumno->unAlumno();
		$alumnos=$alumno->alumnosClases();
		require_once 'views/relaciones/crear.php';
	}

	public function eliminar(){
		if (isset($_GET['id_relacion']) && $_SESSION['id']){
			$id=$_SESSION['id'];
			$id_relacion=$_GET['id_relacion'];
			$relacion= new relacion();
			$relacion->setId_relacion($id_relacion);
			$delete=$relacion->eliminar();
			if ($delete) {
				$_SESSION['delete']='complete';
			}else{
				$_SESSION['delete']='failed';
			}
		}else{
			$_SESSION['delete']='failed';
		}
		header("Location:".base_url."alumno/relaciones&id_alumno=$id");
	}

	public function asignacion(){
		$id=$_SESSION['id'];
		if (isset($_POST)) {

			$contacto = isset($_POST['id_alumno']) ? $_POST['id_alumno'] : false;
			$relacionbm = isset($_POST['relacion']) ? $_POST['relacion'] : false;
			
			//var_dump($relacionbm);
			//die();

			if($contacto && $relacionbm){
				$relacions= new relacion();
				$relacions->setId_alumno($_SESSION['id']);
				$relacions->setId_alumnodos($contacto);
				$relacions->setRelacion($relacionbm);
				$save= $relacions->save();
			
				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "failed";
		}
		
		header("Location:".base_url."alumno/relaciones&id_alumno=$id");
	}

} 
?>