<?php
require_once "models/posicion.php";
require_once "models/grupo.php";
require_once "models/aula.php";
require_once "models/asignatura.php";
require_once "models/alumno.php";

class posicionController{

    public function guardado(){
        $id_grupo=$_GET['id_grupo'];
        require_once 'views/profesor/navegacion.php';
        require_once "views/posicion/guardar.php";
    }
    public function guardar(){
        $posiciones=new posicion();
        $id_grupo=$_GET['id_grupo'];
        $nombre=$_POST['nombre'];
        $alu=$posiciones->CapturarGrupo($id_grupo);
        while ($alumno=$alu->fetch_object()) {
            $posg=new posicion();
            $posg->setId_alumno($alumno->id_alumno);
            $x=$posg->xAlumno();
            $posg->setCoordx($x);
            $y=$posg->yAlumno();
            $posg->setCoordy($y);
            $posg->setGuardado($nombre);
            $posg->guardad();
            
        }
        header("Location:".base_url.'aula/verT');
    }

    public function carga(){
        $id_grupo=$_GET['id_grupo'];
        $grupo=new grupo();
        $grupo->setId_grupo($id_grupo);
        $guardados=$grupo->alumnosGuardado();
        require_once 'views/profesor/navegacion.php';
        require_once "views/posicion/cargar.php";
    }
    public function borrar(){
        $id_grupo=$_GET['id_grupo'];
        $grupo=new grupo();
        $grupo->setId_grupo($id_grupo);
        $guardados=$grupo->alumnosGuardado();
        require_once 'views/profesor/navegacion.php';
        require_once "views/posicion/eliminar.php";
    }
    
}
?>