<?php
require_once 'models/grupo.php';
require_once 'models/asignatura.php';

class grupoController{

	public function grupos(){
		Utils::isAdmin();
		$grupo=new Grupo();
		$grupos=$grupo->grupos();
		$grupocon=$grupo->grupoCon();

		require_once 'views/profesor/navegacion.php';
		require_once 'views/grupo/gruposin.php';
		require_once 'views/grupo/grupos.php';

	}
	public function miGrupo(){
		Utils::isTutor();
		$id_tutor=$_SESSION['identity']->id_profesor;
		$grupo=new grupo;
		$grupo->setId_tutor($id_tutor);
		$id=$grupo->grupoTutor();
		$id_grupo=$id->fetch_object();
		$grupo->setId_grupo($id_grupo);
		$alumnos=$grupo->alumnosTutoria();
	
		require_once 'views/profesor/navegacion.php';
		require_once 'views/alumno/alumnos.php';
	}


	public function asignar(){
		Utils::IsAdmin();
		if (isset($_POST)) {
			$grupo = isset($_POST['grupo']) ? $_POST['grupo'] : false;
			$aula=isset($_POST['aula']) ? $_POST['aula'] : false;
			$id_tutor=$_GET['id_profesor'];

			if($grupo && $aula && $id_tutor){
				$grupot= new Grupo();
				$grupot->setId_grupo($grupo);
				$grupot->setId_tutor($id_tutor);
				$tutoria=new asignatura();
				$tutoria->setNombre("tutoria");
				$tutoria->setId_aula($aula);
				$tutoria->setId_profesor($id_tutor);
				$tutoria->setId_grupo($grupo);
				$save1=$grupot->tutorGrupo();
				$save2=$tutoria->crearAsignatura();
			

				if ($save1 && $save2) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
	header("Location:".base_url.'profesor/profesores');

	}


	public function crear(){
		Utils::isAdmin();
		require_once 'views/profesor/navegacion.php';
		require_once 'views/grupo/crear.php';
		

	}

	public function save(){
		Utils::isAdmin();
		if (isset($_POST)) {
			
			$grupon = isset($_POST['nombreGrupo']) ? $_POST['nombreGrupo'] : false;

			if($grupon){
				$cero=0;
				$grupo= new Grupo();
				$grupo->setnombreGrupo($grupon);
				$grupo->setId_tutor($cero);
				
				$save= $grupo->save();
				
				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		header("Location:".base_url.'grupo/grupos');
	}


	public function borrar(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$grupo= new Grupo();
				$grupo->setId_grupo($id);
				$delete=$grupo->borrarGrupo();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."grupo/grupos");
	}

	
}

?>