<?php 

require_once 'models/asignatura.php';
require_once 'models/grupo.php';
require_once 'models/aula.php';
require_once 'models/profesor.php';

class asignaturaController{
	function ver(){
		$asignatura=new asignatura();
		$asignatura->setId_profesor($_SESSION['identity']->id_profesor);
		$registros=$asignatura->getAsignaturas();
		require_once 'views/profesor/navegacion.php';
		if (isset($_SESSION['admin'])) {
				require_once 'views/asignatura/asignaturadmin.php';	
			}elseif(isset($_SESSION['tutor'])){
				require_once 'views/asignatura/asignaturaprof.php';
			}else{
				require_once 'views/asignatura/asignaturaprof.php';
			}
	}

	function tutoria(){
		$asignatura=new asignatura();
		$id_tutor=$_SESSION['identity']->id_profesor;
		$grupo=new grupo();
		$grupo->setId_tutor($id_tutor);
		$gru=$grupo->grupoTutor()->fetch_object();
		$id_grupo=$gru->id_grupo;
		$ngrupo=$gru->nombreGrupo;
		$asignatura->setId_grupo($id_grupo);
		$registros=$asignatura->getTutoria();
		Utils::isTutor();
		require_once 'views/profesor/navegacion.php';
		require_once 'views/asignatura/tutoria.php';
	}

	function creacion(){
		Utils::isTutor();
		$aula=new aula();
		$aulas=$aula->aulas();
		$profesor=new profesor();
		$profesores=$profesor->getAlll();
		$id_grupo=$_GET['id_grupo'];
		require_once 'views/profesor/navegacion.php';
		require_once 'views/asignatura/crear.php';
	}

	function crearAsignatura(){
		Utils::isTutor();
		if (isset($_POST)) {
			$nombre= isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$grupo = isset($_POST['id_grupo']) ? $_POST['id_grupo'] : false;
			$id_aula=isset($_POST['id_aula']) ? $_POST['id_aula'] : false;
			$id_profesor=isset($_POST['id_profesor']) ? $_POST['id_profesor'] : false;
			

			if($nombre && $grupo && $id_profesor && $id_aula){
				$asignatura=new Asignatura();
				$asignatura->setNombre($nombre);
				$asignatura->setId_grupo($grupo);
				$asignatura->setId_aula($id_aula);
				$asignatura->setId_profesor($id_profesor);
				$save=$asignatura->crearAsignatura();

				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		header("Location:".base_url.'asignatura/tutoria');
	}

	function eliminar(){
		Utils::isTutor();
		$asignatura=new asignatura();
		$id=$_GET['id'];
		$asignatura->setId($id);
		$asignatura->eliminar();
		header("Location:".base_url.'asignatura/tutoria');
	}
	


}

?>