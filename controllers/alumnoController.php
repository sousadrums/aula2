<?php 

require_once 'models/alumno.php';
require_once 'models/relacion.php';
require_once 'models/grupo.php';
require_once 'models/posicion.php';
class alumnoController{


    public function index(){
		if (isset($_SESSION['id'])) {
			unset($_SESSION['id']);
		}
		$alumno=new alumno();
		$alumnos=$alumno->alumnosClases();
		require_once 'views/profesor/navegacion.php';
		require_once 'views/alumno/alumnos.php';
	}

	public function relaciones(){
		$id=$_GET['id_alumno'];
		if (!isset($_SESSION['alumno'])) {
			$_SESSION['id']=$id;
		}
		$relacion=new relacion();
		$relaciones=$relacion->relaciones();
		require_once 'views/relaciones/ver.php';
	}
	public function save(){
		
		if (isset($_POST)) {
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
            $nacimiento = isset($_POST['nacimiento']) ? $_POST['nacimiento'] : false;


			if($nombre && $apellidos && $nacimiento){
				$alumno= new Alumno();
				$alumno->setNombre($nombre);
				$alumno->setApellidos($apellidos);
				$alumno->setNacimiento($nacimiento);

				//guardar imagen
				if(!isset($_POST['foto'])){
					$filename="MiiAvatar.png";
					$alumno->setfoto($filename);
				}else{
					$file=$_FILES['foto'];
					$filename=$file['name'];
					$mimetype=$file['type'];

				

					if ($mimetype=="image/jpeg" || $mimetype=="image/jpg" || $mimetype== "image/jpg"|| $mimetype== "image/png" || $mimetype== "image/gif") {
						if (!is_dir('uploads/images')) {
							mkdir('uploads/images', 0777, true);
						}
						move_uploaded_file($file['tmp_name'], 'uploads/images/'.$filename);
						$alumno->setFoto($filename);
					}
				}

			$save= $alumno->save();
			
			if ($save) {
				$_SESSION['register']= "complete" ;
			}else{
				$_SESSION['register']= "failed";
			}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		
		header("Location:".base_url.'alumno/index');
	}
	public function saveTut(){
		
		if (isset($_POST)) {
			
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
            $nacimiento = isset($_POST['nacimiento']) ? $_POST['nacimiento'] : false;
			$id_tutor=$_SESSION['identity']->id_profesor;
			$grupo=new grupo();
			$grupo->setId_tutor($id_tutor);
			$id=$grupo->grupoTutor();
			$id_grup=$id->fetch_object();
			$id_grupo=$id_grup->id_grupo;

			if($nombre && $apellidos && $nacimiento && $id_grupo){
				$alumno= new alumno();
				$alumno->setNombre($nombre);
				$alumno->setApellidos($apellidos);
				$alumno->setNacimiento($nacimiento);
				$alumno->setId_grupo($id_grupo);

				//guardar imagen
				if(!isset($_POST['foto'])){
					$filename="MiiAvatar.png";
					$alumno->setfoto($filename);
				}else{
					$file=$_FILES['foto'];
					$filename=$file['name'];
					$mimetype=$file['type'];

				

					if ($mimetype=="image/jpeg" || $mimetype=="image/jpg" || $mimetype== "image/jpg"|| $mimetype== "image/png" || $mimetype== "image/gif") {
						if (!is_dir('uploads/images')) {
							mkdir('uploads/images', 0777, true);
						}
						move_uploaded_file($file['tmp_name'], 'uploads/images/'.$filename);
						$alumno->setFoto($filename);
					}
				}


			$save= $alumno->saveTut();
			
			if ($save) {
				$_SESSION['register']= "complete" ;
			}else{
				$_SESSION['register']= "failed";
			}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		
		header("Location:".base_url.'grupo/miGrupo');
	}

	function crear(){
			 require_once 'views/alumno/crear.php';
	}

	function borrar(){
		if (isset($_GET['id_alumno'])) {
			$id_alumno=$_GET['id_alumno'];
			$alumno= new alumno();
			$alumno->setId_alumno($id_alumno);
			$delete=$alumno->borrarAlumno();
			if ($delete) {
				$_SESSION['delete']='complete';
			}else{
				$_SESSION['delete']='failed';
			}

		}else{
			$_SESSION['delete']='failed';
		}


		header("Location:".base_url."alumno/index");
	}
	function borrarg(){
		if (isset($_GET['id_alumno'])) {
			$id_alumno=$_GET['id_alumno'];
			$alumno= new alumno();
			$alumno->setId_alumno($id_alumno);
			$delete=$alumno->borrarAlumno();
			if ($delete) {
				$_SESSION['delete']='complete';
			}else{
				$_SESSION['delete']='failed';
			}

		}else{
			$_SESSION['delete']='failed';
		}


		header("Location:".base_url."grupo/miGrupo");
	}


	function asignar(){
			if (isset($_GET['id_alumno'])) {
				$id_alumno=$_GET['id_alumno'];
				if (!isset($_SESSION['alumno'])) {
					$_SESSION['id_alumno']=$id_alumno;
				}
				$alumno= new alumno();
				$alumno->setId_alumno($id_alumno);
				$grupos=$alumno->grupos();
				require_once 'views/alumno/asignar.php';
			}
		
		
	}


	function escogerGrupo(){
		if (isset($_SESSION['id_alumno']) && isset($_POST['id_grupo'])) {
			$id_alumno=$_SESSION['id_alumno'];
			$id_grupo=$_POST['id_grupo'];
			$alumno= new alumno();
			$alumno->setId_alumno($id_alumno);
			$alumno->setId_grupo($id_grupo);
			$alumno->asignarGrupo();
			unset($_SESSION['id_alumno']);	
		}
		header("Location:".base_url."alumno/index");
	}

	

	function popOver(){
		require_once 'views/alumno/pop.php';		
	}




}

?>