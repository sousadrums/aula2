<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Alumnos</h5>
		<a class="btn btn-primary mb-2" href="<?=base_url?>alumno/crearTut">Añadir</a>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>Nombre y apellidos</th>
					<th>Foto</th>
					
					<th>Relaciones</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($alumno=$alumnos->fetch_object()) : ?>
				
					<tr>
						<td><?=$alumno->nombre; ?> <?=$alumno->apellidos; ?></td>
						<td><img class="retrato" src="<?=base_url?>uploads/images/<?=$alumno->foto?>" alt="Imagen"></td>
						<td>
							<a class="btn btn-success btn-sm" href="<?=base_url?>alumno/relaciones&id_alumno=<?=$alumno->id_alumno?>">Editar</a>
							<!--<div class="buenas"><?=$alumno->amigos?></div>
							<div class="malas"><?=$alumno->enemigos?></div>-->
							<a class="btn btn-primary btn-sm pop" data-toggle="popover" title="<?=$alumno->nombre?> <?=$alumno->apellidos?>" data-content="<div class='buenas'><?=$alumno->amigos?></div><div class='malas'><?=$alumno->enemigos?></div>">ver</a>
						</td>
						<td><a class="btn btn-danger btn-sm" href="<?=base_url?>alumno/borrar&id_alumno=<?=$alumno->id_alumno?>">Eliminar</a></td>
					</tr>
				
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>
