<?php if(isset($_SESSION['admin'])) :?>
	<div class="col">	
		<div class="jumbotron">
			<h1>Nuevo alumno</h1>
			<form action="<?=base_url?>alumno/save" method="POST" enctype="multipart/form-data">
					<input class="form-control" placeholder="nombre" type="text" name="nombre" required /><br>
					<input class="form-control" placeholder="apellidos" type="text" name="apellidos" required /><br>
					<input class="form-control" placeholder="nacimiento" type="date" name="nacimiento" required /><br>
					<input class="form-control" placeholder="foto" type="file" name="foto"/><br>
					<input type="submit" class="btn btn-primary" value="Registrar" />
			</form>
		</div>
	</div>
<?php elseif(isset($_SESSION['tutor'])) :?>
	<div class="col">	
		<div class="jumbotron">
			<h1>Nuevo alumno</h1>
			<form action="<?=base_url?>alumno/saveTut" method="POST" enctype="multipart/form-data">
					<input class="form-control" placeholder="nombre" type="text" name="nombre" required /><br>
					<input class="form-control" placeholder="apellidos" type="text" name="apellidos" required /><br>
					<input class="form-control" placeholder="nacimiento" type="date" name="nacimiento" required /><br>
					<input class="form-control" placeholder="foto" type="file" name="foto" /><br>
					<input type="submit" class="btn btn-primary" value="Registrar" />
			</form>
		</div>
	</div>
<?php endif ;?>