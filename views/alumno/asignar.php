<form action="<?=base_url?>alumno/escogerGrupo" method="POST" class="form-group">
	<select name="id_grupo" class="form-control-sm">
		<?php while ($grupo=$grupos->fetch_object()) : ?>
			<option value="<?=$grupo->id_grupo?>"><?=$grupo->nombreGrupo?>
			</option>
		<?php endwhile; ?>
	</select>
	<button type="submit" class="btn btn-primary btn-sm">asignar</button>
</form>