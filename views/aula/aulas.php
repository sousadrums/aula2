<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Aulas</h5>
		<a class="btn btn-primary mb-2" href="<?=base_url?>aula/crear">Añadir</a>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>aula</th>
					<th>Mesas</th>
					
					<th>acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($aula=$aulas->fetch_object()) : ?>
					<tr>
						<td><?=$aula->codigo; ?></td>
						<td><?=$aula->mesas ?></td>
						<td>
							<a class="btn btn-danger btn-sm" href="<?=base_url?>aula/borrar&id_aula=<?=$aula->id_aula?>">Borrar</a>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>