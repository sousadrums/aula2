<?php $clasex=$clase->fetch_object()?>
	<div class="row p-3">
		<div class="col">
			<h1>Aula <?=$clasex->codigo?></h1>
		</div>
	<?php if(isset($_SESSION['admin'])) :?>
	<?php elseif(isset($_SESSION['tutor']) && $_SESSION['identity']->id_profesor==$id_tutor) :?>
	
		<div class="col">
			<h2>Configuraciones</h2>
			<a class="btn btn-success btn-sm" href="<?=base_url?>posicion/guardado&id_grupo=<?=$id_grupo?>">guardar</a>
			<a class="btn btn-primary btn-sm" href="<?=base_url?>posicion/carga&id_grupo=<?=$id_grupo?>">cargar</a>
			<a class="btn btn-danger btn-sm" href="<?=base_url?>posicion/borrar&id_grupo=<?=$id_grupo?>">eliminar</a>
		</div>
	<?php elseif(isset($_SESSION['profe']) || isset($_SESSION['tutor'])) :?>
	<?php endif ;?>
	</div>
	<?php if(isset($_SESSION['tutor']) && $_SESSION['identity']->id_profesor==$id_tutor) :?>
		<div class="sala row">
			
			<?php while ($alumno=$alumnos->fetch_object()) :	?>
				<div class="pupitre" id="<?=$alumno->id_alumno?>" style="position:absolute; left:<?=$alumno->coordx?>px; top:<?=$alumno->coordy?>px">					
					<img class="retrato" src="<?=base_url?>uploads/images/<?=$alumno->foto?>" alt="foto">
					<h3 class="texto"><a class="pop" data-toggle="popover" title="<?=$alumno->nombre?> <?=$alumno->apellidos?>" data-content="<div class='buenas'><?=$alumno->amigos?></div><div class='malas'><?=$alumno->enemigos?></div>"><?=$alumno->nombre?> <?=$alumno->apellidos?></a></h3>
					<h5 id="posicion"></h5>
				</div>
			<?php endwhile ;?>
		</div>
	<?php elseif(isset($_SESSION['profe']) || isset($_SESSION['tutor'])) :?>
		<div class="sala row">
				
				<?php while ($alumno=$alumnos->fetch_object()) :	?>
				<div class="pupitre2" id="<?=$alumno->id_alumno?>" style="position:absolute; left:<?=$alumno->coordx?>px; top:<?=$alumno->coordy?>px">					
					<img class="retrato" src="<?=base_url?>uploads/images/<?=$alumno->foto?>" alt="foto">
					<h3 class="texto"><?=$alumno->nombre?> <?=$alumno->apellidos?></h3>
					<h5 id="posicion"></h5>
				</div>
			<?php endwhile ;?>
			</div>
	<?php endif ;?>			
	<br>
	<div class="row">
		<div class="col"></div>
		<div class="col">
			<div class="pizarraborde">
				<div class="pizarralogo">
					<strong>Pizarra</strong>
				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
	<br>
