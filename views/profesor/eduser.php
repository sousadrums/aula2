<div class="col-sm-8">	
	<div  class="jumbotron">
		<h1>Editar datos</h1>
		<form action="<?=base_url?>profesor/update" method="POST">
			<div class="form-group">
				<input type="text" class="form-control" name="usuario" value="<?=isset($usuario)  ? $usuario : '';?>" required /><br>
				<input type="text" class="form-control" name="nombre" value="<?=isset($nombre)  ? $nombre : '';?>"required /><br>	
				<input type="text" class="form-control" name="apellido" value="<?=isset($apellidos)  ? $apellidos : '';?>"required /><br>
				<input type="submit" class="btn btn-primary" value="Guardar" />
			</div>
		</form>
		<br>
		<a href="<?=base_url?>profesor/contraseña" class="button">Cambiar contraseña</a>
	</div>
</div>