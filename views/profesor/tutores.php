<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Tutores</h5>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>Tutor</th>
					<th>grupo</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($profe=$profesores->fetch_object()) : ?>
					
					<tr>
						<td><?=$profe->nombre; ?> <?=$profe->apellidos; ?></td>
						<td><?=$profe->nombreGrupo; ?></td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>
