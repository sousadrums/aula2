<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Profesores</h5>
		<a class="btn btn-primary mb-2" href="<?=base_url?>profesor/register">Añadir</a>
		<table class="table table-hover table-dark table-responsive ">
			<thead>
				<tr>
					<th>Profesor</th>
					<th>rol</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php

				 while ($profe=$profesores->fetch_object()) : ?>
					
					<tr>
						<td><?=$profe->nombre; ?> <?=$profe->apellidos; ?></td>
						<td><?=$profe->rol; ?></td>
						<td class="row">
							<?php if($profe->rol=='tutor') : ?>
								<a class="btn btn-success btn-sm h-1" href="<?=base_url?>profesor/quitarTutoria&id_profesor=<?=$profe->id_profesor?>">Quitar tutoría</a>
							<?php else :?>
								<a class="btn btn-primary btn-sm h-1" href="<?=base_url?>profesor/asignarTutoria&id_profesor=<?=$profe->id_profesor?>">tutor</a>
							<?php endif;?>
						<a class="btn btn-danger btn-sm" href="<?=base_url?>profesor/borrar&id=<?=$profe->id_profesor?>">Eliminar</a>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>
