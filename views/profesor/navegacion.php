<?php if(isset($_SESSION['admin'])) :?>
	<nav class="navbar  navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<h5 class="navbar-brand">Opciones Administrador</h5>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/profesores">Profesores</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/tutores">Tutores</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>aula/aulas">Aulas</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>alumno/index">Alumnos</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>grupo/grupos">Grupos</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/edit">Perfil</a></li>
		</ul>
	</nav>
<?php elseif(isset($_SESSION['tutor'])) :?>
	<nav class="navbar  navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<h5 class="navbar-brand">Opciones tutor</h5>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>asignatura/tutoria">Mi tutoría</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>asignatura/ver">mis asignaturas</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>grupo/miGrupo">mis alumnos</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>aula/verT">mi aula</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/edit">Perfil</a></li>
		</ul>
	</nav>
<?php elseif(isset($_SESSION['profe'])) :?>
	<nav class="navbar  navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<h5 class="navbar-brand">Opciones Profesor</h5>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>asignatura/ver">mis asignaturas</a></li>
			<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/edit">Perfil</a></li>
		</ul>
	</nav>
<?php else : ?>
	<div class="alert alert-danger">
		<p>Usuario y/o contraseña incorrectos. Introduce de nuevo tus credenciales.</p>
	</div>
<?php endif ; ?>		
<br>