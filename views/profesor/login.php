<div class="col">
	<div class="jumbotron">	
		<h1>Inicio de sesión</h1>
		<form action="<?=base_url?>profesor/login" method="POST">
			<div class="form-group">
				<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
				<input type="text" class="form-control" name="usuario" placeholder="Usuario" pattern="[A-Za-z0-9]+" required /><br>
				<input type="password" class="form-control" placeholder="contraseña" name="contraseña" required /><br>
				<input type="submit" class="btn btn-primary" value="Inicio" />
			</div>
		</form>
	</div>
</div>