<?php $profe=$profesor->fetch_object() ?>
<div class="column"><div class="row">
    <h1><?=$profe->nombre?> <?=$profe->apellidos?></h1>
</div>
<div class="row">
    <form action="<?=base_url?>profesor/asignar&id_profesor=<?=$profe->id_profesor?>" method="POST" class="form-group">
        <select name="id_grupo" class="form-control-sm">
            <?php while ($grupo=$gruposin->fetch_object()) : ?>
                <option value="<?=$grupo->id_grupo?>"><?=$grupo->nombreGrupo?>
                </option>
            <?php endwhile; ?>
        </select>
        <select name="id_aula" class="form-control-sm">
            <?php while ($aula=$aulasin->fetch_object()) : ?>
                <option value="<?=$aula->id_aula?>"><?=$aula->codigo?>
                </option>
            <?php endwhile; ?>
        </select>
        <button type="submit" class="btn btn-primary btn-sm">asignar</button>
    </form>
</div>    
</div>