<div class="jumbotron container-fluid">
    <div class="col"></div>
    <div class="col-w-700">
        <div class="politicas">
            <h1>Contacto</h1>
            <h2>Todas las solicitudes deberán dirigirse a la dirección:</h2>
            <h3><b><strong>admin@aula2.xyz</strong></b></h3>
            <ul>
                <li>
                    <h3>Solicitud de información.</h3>
                </li>
                <li>
                    <h3>Solicitud de cuenta de administrador de centro educativo.</h3>
                    <p>El administrador de la plataforma te dará acceso a un formulario de registro de centro.</p>
                </li>
                <li>
                    <h3>Incidencias del funcionamiento de la aplicación.</h3>
                </li>
                <li>
                    <h3>Canal de denuncias</h3>
                    <p>Contacta lo antes posible si observaras cualquier uso indebido por parte de otros usuarios o el incumplimiento de lo que se establece en los <a href="<?=base_url?>legal/uso">Términos de uso.</a></p>
                </li>
                <li>
                    <h3>Sugerencias sobre la plataforma y su funcionalidad.</h3>
                </li>
            </ul>
            <br>
        </div>
    </div>
    <div class="col"></div>
</div>