<div class="jumbotron container-fluid">
    <div class="col"></div>
    <div class="col-w-700">
        <div class="politicas" id="privacidad">
            <h1>Política de privacidad</h1>
            <p>Esta Política de Privacidad trata de dar a conocer qué información se recoge en este sitio web, cómo se usa, cómo se procesa y cómo se comparte</p>

            <p>Por favor, lea el apatado relativo a los términos de uso de este sitio web: <a href="<?=base_url?>legal/uso">Términos de uso.</a></p>
            <h2>Uso de cookies</h2>
            <p>Este sitio web utiliza únicamente aquellas cookies que permiten el funcionamiento y la prestación de los servicios ofrecidos y son, por tanto, exentas de aplicación de lo establecido en el art. 22.2 de la LSSI.</p>
            <p>Dichas cookies son las generadas en el proceso de autenticación para el manejo de la sesión del lado del servidor (proveedor Hostinger.com) y las del servicio de seguridad y entrega de contenido Cloudflare.com.</p>
            <h2>¿Qué información recoge Aula2 sobre mí y como se muestra?</h2>
            <p>La única información personal que almacena Aula2 sobre sus usuarios docentes o administradores de centro educativo y la manera en que se muestra a otros usuarios es la siguiente:</p>
            <ul>
                <li><b>Nombre de usuario:</b> Solo se utiliza a efectos de autenticación de usuario. Se muestra en caso de edición.</li>
                <li><b>Nombre y apellidos:</b> Se muestra a los usuarios y administradores de centro. </li>
            </ul>
            <h2>¿Qué información recoge Aula2 de los alumnos?</h2>
            <h3>Información personal</h3>
            <p>La información que almacena Aula2 es la siguiente:</p>
            <ul>
                <li><b>Nombre y apellidos: </b>Se utiliza para su identificación en el aula y el establecimiento de sus interrelaciones. Se muestra en el aula y en el listado de alumnos.</li>
                <li><b>Fecha de nacimiento: </b>No se muestra. Se utiliza para el calculo de la edad y curso académico.</li>
                <li><b>Fotografía:</b> Es un campo opcional. En caso de no usarse se sustituye por un avatar genérico. Se muestra tanto en el aula como en el listado de alumnos en modo miniatura ampliable para facilitar la identificación del alumno por parte del profesor.</li>
            </ul>
            <p>La información mencionada solo puede ser creada, consultada, editada y/o eliminada por el equipo docente y por el administrador de centro. Este último será quién gestione la información de profesores y tutores.</p>
            <p>Cada tutor podrá gestionar su aula, sus alumnos, las asignaturas del grupo y los profesores que las imparten. Los profesores solo podrán consultar la disposicion de las aulas donde impartan asignaturas.</p>
            <p>Ante cualquier incidencia, el correo de contacto es <b>admin@aula2.xyz</b></p>
        </div>
    </div>
    <div class="col"></div>
</div>