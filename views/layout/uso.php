<div class="jumbotron container-fluid">
    <div class="col"></div>
    <div class="col-w-700">
        <div class="politicas">
            <h1>Términos de uso</h1>
            <p>Aula2 es una aplicación web destinada al docente. Busca ofrecer un servicio de consulta y configuración de la disposicion de los alumnos en el aula con una interfaz sencilla. </p>
            <p>Se establecen dos roles principales, tutor y profesor, así como un rol de administrador que gestiona los dos anteriores. </p>
            <ul>
                <li><b>Tutor:</b> Gestiona los alumnos y su disposición en el aula, así como las asignaturas que cursa su grupo. También asigna estas asignaturas al profesor correspondiente.</li>
                <li><b>Profesor: </b>Consulta la disposición de las aulas en la que imparte sus asignaturas.</li>
            </ul>
            <p> Esta plataforma y su correspondiente dominio pertenece y está gestionada por una persona física residente en territorio español y están hospedadas en el proveedor de alojamiento web hostinger.com.</p>
            <p>La localización de los servidores de hostinger.com que proporcionan el alojamiento web de Aula2 están repartidos por Reino Unido, Estados Unidos, Brasil, Países Bajos, Singapur, Indonesia y Lituania.</p>
            <p>El sercicio web que ofrece Aula2 cuenta con certificado SSL que proporciona una comunicación segura entre el usuario y el servidor a través de la red.</p>
            <p>Por favor, lea el apartado relativo a la <a class="politicas" href="<?=base_url?>legal/privacidad"> poítica de privacidad </a>de este sitio web.</p>
            
            <h2>Acuerdo y conformidad de uso</h2>
            <p>Al usar Aula2, usted afirma que ha leido, entiende y acepta todo lo establecido en estos "Terminos de uso" y de todo lo establecido en la <a class="politicas" href="<?=base_url?>legal/privacidad"> poítica de privacidad</a>.</p>
            <p>Si no está de acuerdo con cualquier punto de los aquí establecidos, por favor, absténgase del uso de Aula2 y solicite la cancelación de su cuenta en <b>admin@aula2.xyz</b></p>

            <h2>Creación de cuenta en Aula2 y mantenimiento</h2>
            <p>Para poder usar Aula2 es necesario disponer de una cuenta de profesor, tutor o administrador de centro educativo.</p>
            <p>Por el momento, la solicitud de cuentas la debe realizar el centro educativo a través de la dirección de correo <b>admin@aula2.xyz</b> donde se debe facilitar el nombre del centro y una abreviatura de éste junto con una dirección de correo educativa o corporativa del que será el administrador de cuentas de docentes del centro.</p>
            <p>Será el usuario con perfil de administrador de centro educativo el encargado de gestionar las cuentas de docentes de su propio centro.</p>
            <p>El administrador de la plataforma Aula2 también podrá realizar la gestión de cuentas de usuario de un centro educativo bajo solicitud expresa a <b>admin@aula2.xyz</b>.</p>
            
            <h2>Normas de uso</h2>
            <h3>Tutores</h3>
            <p>El tutor está sujeto a las siguientes directrices:</p>
            <ul>
                <li>Se encargará de asignar los profesores de las asignaturas de sus alumnos. Para ello, dichos profesores deberán estár dados de alta en la plataforma por el administrador. No está permitido asignar profesores que no hayan aceptado voluntariamente las condiciones de estos "Terminos de uso" y de la <a class="politicas" href="<?=base_url?>legal/privacidad"> poítica de privacidad</a>. De este modo, solo aquellos docentes que hayan solicitado expresamente utilizar esta plataforma podrán hacer uso de ella.</li>
                <li>Es el resposable de la gestión de la información de los alumnos. La proyección del aula o del listado de alumnos en medios del centro educativo queda condicionada a las directrices de protección de datos del centro.</li>
                <li>Está totalmente prohibida la modificación de los datos personales concedidos por el centro atendiendo a la politica de protección de datos de dicho centro. Mas aún si dicha modificación incurriese en discriminaciones, burlas, motes, apelativos o contenidos inapropiados. Así mismo, queda totalmente prohibido la exposición de información que pueda identificar la forma de contacto con cualquiera de los usuarios o alumnos en la plataforma. Cualquier incidencia será supervisada por el administrador de centro y reportada al administrador de la web a traves de <b>admin@aula2.xyz</b>. El administrador de la web velará por el escrupuloso cumplimiento de este punto.</li>
                <li>Las fotografías, en caso de ser utilizadas, tienen la única finalidad de identificar alumnos facilmente por el tutor o profesor. Son almacenadas de manera segura en los servidores de hostinger.com. Queda totalmente prohibido el uso de dichas imágenes para cualquier otro fin que no sea el descrito.</li>
            </ul>
            <p>Cualquier uso inapropiado o contrario a las presentes directrices conllevarán la suspensión inmediata de la cuenta del infractor. El administrador de la web procederá a resolver las incidencias causadas y ejecutará las correspondientes acciones legales, si fuese necesario, contra el infractor.</p>
            <h3>profesores</h3>
            <p>Los profesores acceden a Aula2 con la única finalidad de consulta, no pudiendo modificar más que sus propios datos personales. Sin embargo, por acceder a información sensible de los alumnos (nombre e imagen), quedan sujestos a las mismas restricciones de uso definidas para los tutores.</p>
            <h2>Canal de denuncias</h2>
            <p>Aunque Aula2 exige a todos sus uauarios cumplir con estos términos de uso, cualquier contenido inapropiado siempre puede ser enviado y mostrado en este sitio web. Si algún usuario encontrara algún contenido que violara estas las normas de uso debe comunicarlo de inmediato al administrador de este sitio web a través de la dirección <b>admin@aula2.xyz</b>.</p>

        </div>
    </div>
    <div class="col"></div>
</div>