<div class="jumbotron container-fluid">
    <div class="col"></div>
    <div class="col-w-700">
        <div class="politicas">
            <h1>Acerca de</h1>
            <h2>¿Qué es <img class="logo" src="<?=base_url?>assets/img/auladoslogo.jpg" alt="Aula2">?</h2>
            <p>Aula2 es una plataforma que persigue facilitar la disposición del aula a los tutores y la consulta de esta por parte de los profesores.</p>
            <p>Para ello se puede contar con información de las preferencias de los alumnos por los compañeros que desean tener cerca (o lejos).</p>
            <p>Simplemente, como tutor, arrastra el pupitre de tus alumnos donde deseas colocarlos y listo.</p>
            <p>Es posible guardar distintas disposiciones del aula y cargarlas cuando se requiera.</p>
            <h2>Funcionalidad y características de la plataforma</h2>
            <p>Aula2 se encuentra viva y en constante mejora. Actualmente ofrece las siguientes funcionalidades:</p>
            <ul>
                <li>Registro de profesores y tutores</li>
                <li>Registro y asignación de asignaturas a los profesores.</li>
                <li>Gestión de los alumnos por parte del tutor, así como de las preferencias de los alumnos por su entorno de compañeros. </li>
                <li>Consulta de la disposición de la clase por parte de los profesores.</li>
            </ul>
            <h2>Protección de datos de carácter personal</h2>
            <p>Tanto el diseño de la plataforma Aula2 como su <a class="politicas" href="<?=base_url?>legal/privacidad"> poítica de privacidad </a> y sus <a href="<?=base_url?>legal/uso">Términos de uso</a> han sido creados para proteger la privacidad y la información personal de los profesores y alumnos.</p>
            <br>
        </div>
    </div>
    <div class="col"></div>
</div>