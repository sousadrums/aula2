
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="<?=base_url?>assets/css/styles.css?v<?php echo(rand()); ?>" />
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans&display=swap" rel="stylesheet">
	<meta name="title" content="Aula2. Distribuye tu aula">
	<meta name="Description" content="Aplicación destinada a gestionar los pupitres del aula de tu centro">
	<meta name="google-site-verification" content="w9VtmUdsxmD7wzdFCQ0T4E5S1fLbWjpq8VAfM5fvzKI" />
	<meta name="Keywords" content="Aula, pupitres, instituto, clase, tutor, asignatura, gestión">
	<title>aula2</title>
	<link rel="icon" type="image/x-icon" href="<?=base_url?>assets/img/auladoslogo.jpg" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.theme.min.css">
	<script type="text/javascript" src="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?=base_url?>assets/js/interface.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-BG84NWPK2N"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-BG84NWPK2N');
	</script>
</head>
<body>
	<div class="container">
		<!--Cabecera-->
		<nav class="navbar  navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="<?=base_url?>info/inicio"><img class="logo" src="<?=base_url?>assets/img/auladoslogo.jpg" alt="Aula2"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
   				<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav">
					<?php if(!isset($_SESSION['identity'])): ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/loginV">Inicio sesión</a></li>
					<?php else: ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/loged"><?=$_SESSION['identity']->nombre?> <?=$_SESSION['identity']->apellidos?> </a></li>
						
						<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/logout">Cerrar sesión</a></li>
					<?php endif; ?>
					
					<li class="nav-item"><a class="nav-link" href="<?=base_url?>info/contact">Contacto</a></li>
					<li class="nav-item"><a class="nav-link" href="<?=base_url?>info/about">Acerca de</a></li>
				</ul>
			</div>
		</nav>
	<br>
	<!--cuerpo-->
	<div class="container">