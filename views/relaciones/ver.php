
<div>
	<a class="btn btn-success btn-sm" href="<?=base_url?>relacion/crear">añadir</a>
	<hr>
	<table class="table table-hover table-dark table-responsive-sm">
		<?php while($relacion=$relaciones->fetch_object()) : ?>
			<?php if ($relacion->identificador==$id) : ?>
				<?php if ($relacion->tipo==1) : ?>
				<tr>
				<td class="buenas">
					<?=$relacion->nombreAmigo?> <?=$relacion->apellidoAmigo?>
				</td>
				<td>
					<a class="btn btn-danger btn-sm" href="<?=base_url?>relacion/eliminar&id_relacion=<?=$relacion->id_relacion?>">eliminar</a>
				</td>
				</tr>
			<?php else : ?>
				<tr>
				<td class="malas">
					<?=$relacion->nombreAmigo?> <?=$relacion->apellidoAmigo?>
				</td>
				<td>
					<a class="btn btn-danger btn-sm"href="<?=base_url?>relacion/eliminar&id_relacion=<?=$relacion->id_relacion?>">eliminar</a>
				</td>
				</tr>
			<?php endif ; ?>
		<?php endif ; ?>
		<?php endwhile ; ?>
	</table>
	<?php if(isset($_SESSION['admin'])) :?>
		<a class="btn btn-primary btn-sm" href="<?=base_url?>alumno/index">volver</a>
	<?php elseif(isset($_SESSION['tutor'])) : ?>
		<a class="btn btn-primary btn-sm" href="<?=base_url?>grupo/miGrupo">volver</a>
	<?php endif ; ?>
</div>


