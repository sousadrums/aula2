<div class="col" >
	<div class="container pt-3 pb-3 border">
		<div>
			<h5>Asignaturas de mi grupo <?=$ngrupo?></h5>
			<a href="<?=base_url?>asignatura/creacion&id_grupo=<?=$id_grupo?>" class="btn btn-primary btn-sm">Crear</a>
			<hr>
		</div>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>asignatura</th>
					<th>profesor</th>
					<th>aula</th>
					<th>acceso</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($reg=$registros->fetch_object()) : ?>
					<tr>
						<td><?=$reg->asignatura; ?></td>
						<td><?=$reg->nombrep; ?> <?=$reg->apellidosp; ?></td>
						<td><?=$reg->aula; ?></td>
						<td>
							<a href="<?=base_url?>asignatura/eliminar&id=<?=$reg->id?>" class="btn btn-danger btn-sm">Eliminar</a>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>