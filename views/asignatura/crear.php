<div class="col">	
	<div class="jumbotron">
		<h1>Crear asignatura</h1>
		<form action="<?=base_url?>asignatura/crearAsignatura" method="POST">
                <input type="hidden" name="id_grupo" value="<?=$id_grupo ?>">
				<input class="form-control" placeholder="nombre" type="text" name="nombre" required /><br>
                <label for="profesor">Profesor</label>
                <select name="id_profesor" class="form-control-sm">
                    <?php while ($profesor=$profesores->fetch_object()) : ?>
                        <option value="<?=$profesor->id_profesor?>"><?=$profesor->nombre?> <?=$profesor->apellidos?> 
                        </option>
                    <?php endwhile; ?>
                </select><br>
                <label for="Aula">Aula</label>
                <select name="id_aula" class="form-control-sm">
                    <?php while ($aula=$aulas->fetch_object()) : ?>
                        <option value="<?=$aula->id_aula?>"><?=$aula->codigo?>
                        </option>
                    <?php endwhile; ?>
                </select><br>
                <hr>
				<input type="submit" class="btn btn-primary" value="Crear" />
		</form>
	</div>
</div>