<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Mis asignaturas</h5>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>asignatura</th>
					<th>grupo</th>
					<th>aula</th>
					<th>acceso</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($reg=$registros->fetch_object()) : ?>
					<tr>
						<td><?=$reg->asignatura; ?></td>
						<td><?=$reg->grupo; ?></td>
						<td><?=$reg->aula; ?></td>
						<td>
							<a href="<?=base_url?>aula/ver&id_aula=<?=$reg->id_aula?>" class="btn btn-primary">Aula</a>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>
