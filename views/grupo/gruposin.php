<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Grupos</h5>
		<a class="btn btn-primary mb-2" href="<?=base_url?>grupo/crear">Crear grupo</a>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>grupos sin tutor</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($grupo=$grupos->fetch_object()) : ?>
					<tr>
						<td><?=$grupo->nombreGrupo; ?></td>
						<td><a class="btn btn-danger btn-sm" href="<?=base_url?>grupo/borrar&id=<?=$grupo->id_grupo?>">Eliminar</a></td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>
