<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Grupos asignados</h5>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>grupo</th>
					<th>Tutor</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($grupo=$grupocon->fetch_object()) : ?>
					<tr>
						<td>
							<?=$grupo->nombreGrupo; ?>
						</td>
						<td>
							<?=$grupo->nombre?> <?=$grupo->apellidos?>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>