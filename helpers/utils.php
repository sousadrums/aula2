<?php

class Utils{
	
	public static function deleteSession($name){
		if(isset($_SESSION[$name])){
			$_SESSION[$name] = null;
			unset($_SESSION[$name]);
		}
		
		return $name;
	}
	
	public static function isAdmin(){
		if(!isset($_SESSION['admin'])){
			session_destroy();
			header("Location:".base_url);
		}else{
			return true;
		}
	}

	public static function isTutor(){
		if(!isset($_SESSION['tutor'])){
			session_destroy();
			header("Location:".base_url);
		}else{
			return true;
		}
	}
	
	public static function isProfe(){
		if(!isset($_SESSION['profe'])){
			session_destroy();
			header("Location:".base_url);
		}else{
			return true;
		}
	}

	public static function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	  }
	
}
