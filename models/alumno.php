<?php


class alumno{
	private $id_alumno;
	private $nombre;
	private $apellidos;
	private $nacimiento;
	private $foto;
	private $id_grupo;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
		}

	function getId_alumno(){
		return $this->id_alumno;
	}

	function getNombre(){
		return $this->nombre;
	}

	function getApellidos(){
		return $this->apellidos;
	}

	function getNacimiento(){
		return $this->nacimiento;
	}
	function getFoto(){
		return $this->foto;
	}
	function getId_grupo(){
		return $this->id_grupo;
	}



	function setId_alumno($id_alumno){
		$this->id_alumno=$id_alumno;
	}
	function setNombre($nombre){
		$this->nombre=$this->db->real_escape_string($nombre);
	}
	function setApellidos($apellidos){
		$this->apellidos=$this->db->real_escape_string($apellidos);
	}
	function setNacimiento($nacimiento){
		$this->nacimiento=$nacimiento;
	}
	function setFoto($foto){
		$this->foto=$foto;
	}
	function setId_grupo($id_grupo){
		$this->id_grupo=$id_grupo;
	}

	
	public function save(){
		$sql = "INSERT INTO alumnos VALUES (NULL, '{$this->getNombre()}', '{$this->getApellidos()}','{$this->getNacimiento()}', '{$this->getFoto()}', NULL)";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	public function saveTut(){
		$sql = "INSERT INTO alumnos VALUES (NULL, '{$this->getNombre()}', '{$this->getApellidos()}','{$this->getNacimiento()}', '{$this->getFoto()}', '{$this->getId_grupo()}')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function relaciones(){
		$sql="SELECT relaciones.id_alumno AS identificador, relaciones.id_alumnodos, alumnos.nombre AS nombreAmigo, alumnos.apellidos AS apellidoAmigo, relaciones.relacion AS tipo FROM relaciones JOIN alumnos ON relaciones.id_alumnodos=alumnos.id_alumno ";
		$alumnos= $this->db->query($sql);
		
		return $alumnos;
	}

	public function alumnos(){
		$sql="SELECT * FROM alumnos";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}

	public function unAlumno(){
		$sql="SELECT * FROM alumnos WHERE id_alumno={$this->id_alumno}";
		$alumno= $this->db->query($sql);
		return $alumno;
	}
	
	public function alumnosClases(){
		$sql="SELECT * ,(SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=1) AS amigos, (SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=2) AS enemigos FROM alumnos LEFT JOIN grupos ON grupos.id_grupo=alumnos.id_grupo ORDER BY id_alumno";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}

	public function alumnosSinGrupo(){
		$sql="SELECT * FROM alumnos WHERE id_grupo IS NULL";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}

	public function borrarAlumno(){
		$sql="DELETE FROM alumnos WHERE id_alumno={$this->id_alumno}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	public function alumnosClase(){
		$sql="SELECT * ,(SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=1) AS amigos, (SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=2) AS enemigos, (SELECT p.coordx FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno AND p.guardado='sin') AS coordx, (SELECT p.coordy FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno AND guardado='sin') AS coordy FROM alumnos INNER JOIN grupos ON grupos.id_grupo=alumnos.id_grupo WHERE alumnos.id_grupo={$this->getId_grupo()}";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}
	public function alumnosClaseg($guardado){
		$sql="SELECT * ,(SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=1) AS amigos, (SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=2) AS enemigos, (SELECT p.coordx FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno AND p.guardado='$guardado') AS coordx, (SELECT p.coordy FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno AND guardado='$guardado') AS coordy FROM alumnos INNER JOIN grupos ON grupos.id_grupo=alumnos.id_grupo WHERE alumnos.id_grupo={$this->getId_grupo()}";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}
	

	function asignarGrupo(){
		$sql="UPDATE alumnos SET id_grupo={$this->getId_grupo()} WHERE id_alumno='{$this->getId_alumno()}'";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	

	function grupos(){
		$sql="SELECT * FROM grupos";
		$grupos= $this->db->query($sql);
		return $grupos;
	}
	function grupo(){
		$sql="SELECT * FROM grupos INNER JOIN alumnos ON grupos.id_grupo=alumnos.id_grupo ";
		$aula= $this->db->query($sql);
		return $aula;
	}
}
?>