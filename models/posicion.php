<?php 

Class posicion{

	private $id_posicion;
	private $id_alumno;
	private $coordx;
	private $coordy;
	private $guardado;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
		}

	function getId_posicion(){
		return $this->id_posicion;
	}

	function getId_alumno(){
		return $this->id_alumno;
	}

	function getCoordx(){
		return $this->coordx;
	}

	function getCoordy(){
		return $this->coordy;
	}

	function getGuardado(){
		return $this->guardado;
	}


	function setId_posicion($id_posicion){
		$this->id_posicion=$id_posicion;
	}

	function setId_alumno($id_alumno){
		$this->id_alumno=$id_alumno;
	}

	function setCoordx($coordx){
		$this->coordx=$coordx;
	}

	function setCoordy($coordy){
		$this->coordy=$coordy;
	}

	function setGuardado($guardado){
		$this->guardado=$this->db->real_escape_string($guardado);
	}

	function crearPosicion(){
		$sql = "INSERT INTO posiciones VALUES (NULL, {$this->getId_alumno()}, {$this->getCoordx()}, {$this->getCoordy()}, 'sin')";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	function CapturarGrupo($id_grupo){
		$sql="SELECT id_alumno FROM alumnos WHERE id_grupo={$id_grupo}";
		$alu= $this->db->query($sql);
		return $alu;
	}
	function CapturarPos($id_grupo){
		$sql="SELECT * FROM alumnos WHERE id_grupo={$id_grupo} And guardado={$this->getGuardado()}";
		$alu= $this->db->query($sql);
		return $alu;
	}
	function guardad(){
		$sql = "INSERT INTO posiciones VALUES (NULL, '{$this->getId_alumno()}', '{$this->getCoordx()}', '{$this->getCoordy()}', '{$this->getGuardado()}')";
		$this->mover();
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function mover(){
		/*$sql="UPDATE posiciones SET coordx={$this->getCoordx()}, coordy={$this->getCoordy()} WHERE id_alumno={$this->getId_alumno()} AND guardado='sin'";
		$sql="CASE WHEN id_alumno!='null' THEN UPDATE posiciones SET coordx={$this->getCoordx()}, coordy={$this->getCoordy()} WHERE id_alumno={$this->getId_alumno()} AND guardado='sin' ELSE INSERT INTO posiciones VALUES (NULL, {$this->getId_alumno()}, {$this->getCoordx()}, {$this->getCoordy()}, 'sin') END";
		$save=$this->db->query($sql);
		$result=false;
		if($save){
			$result=true;
		}
		return $result;*/
		
		
		$sql1="DELETE FROM posiciones WHERE id_alumno={$this->id_alumno} AND guardado='sin'";
		$sql2 = "INSERT INTO posiciones VALUES (NULL, {$this->getId_alumno()}, {$this->getCoordx()}, {$this->getCoordy()}, 'sin')";
		$save1= $this->db->query($sql1);
		$save2= $this->db->query($sql2);
		$result=false;
		if ($save1 && $save2) {
			$result=true;
		}
		return $result;
	}

	function posiciones(){
		$sql="SELECT * FROM posiciones WHERE guardado='sin'";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	
	function xAlumno(){
		$sql="SELECT coordx FROM posiciones WHERE guardado='sin' AND id_alumno={$this->id_alumno}";
		$x= $this->db->query($sql);
		$sx=$x->fetch_object()->coordx;
		return $sx;
	}
	function yAlumno(){
		$sql="SELECT coordy FROM posiciones WHERE guardado='sin' AND id_alumno={$this->id_alumno}";
		$y= $this->db->query($sql);
		$sy=$y->fetch_object()->coordy;
		return $sy;
	}
	function cargar($id_grupo){
		$sql="SELECT * FROM posiciones INNER JOIN alumnos a ON a.id_alumno=p.id_alumno WHERE guardado={$this->getGuardado()} AND id_grupo=$id_grupo";
		$posiciones= $this->db->query($sql);
		return $posiciones;
	}
}



 ?>