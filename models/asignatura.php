<?php


class asignatura{
	private $id;
	private $nombre;
	private $id_profesor;
	private $id_grupo;
	private $id_aula;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}


	function getId(){
		return $this->id;
	}

	function getNombre(){
		return $this->nombre;
	}

	function getId_profesor(){
		return $this->id_profesor;
	}

	function getId_grupo(){
		return $this->id_grupo;
	}

	function getId_aula(){
		return $this->id_aula;
	}

	function setId($id){
		$this->id=$id;
	}

	function setNombre($nombre){
		$this->nombre=$this->db->real_escape_string($nombre);
	}

	function setId_profesor($id_profesor){
		$this->id_profesor=$id_profesor;
	}

	function setId_grupo($id_grupo){
		$this->id_grupo=$id_grupo;
	}

	function setId_aula($id_aula){
		$this->id_aula=$id_aula;
	}

	public function getAsignaturas(){
		$id_profesor=intval($this->getId_profesor());
		$sql="SELECT a.nombre asignatura, l.codigo aula, a.id_asignatura id, g.nombreGrupo grupo, l.id_aula id_aula FROM asignaturas a INNER JOIN aulas l ON a.id_aula=l.id_aula  INNER JOIN grupos g ON a.id_grupo=g.id_grupo WHERE a.id_profesor=$id_profesor AND a.nombre<>'tutoria'";
		$asignaturas= $this->db->query($sql);
		return $asignaturas;
	}

	public function getTutoria(){
		$id_grupo=intval($this->getId_grupo());
		$sql="SELECT a.nombre asignatura, l.codigo aula, a.id_asignatura id, g.nombreGrupo grupo, l.id_aula id_aula, p.nombre nombrep, p.apellidos apellidosp FROM asignaturas a INNER JOIN aulas l ON a.id_aula=l.id_aula  INNER JOIN grupos g ON a.id_grupo=g.id_grupo INNER JOIN profesores p ON p.id_profesor=a.id_profesor WHERE a.id_grupo=$id_grupo";
		$asignaturas= $this->db->query($sql);
		return $asignaturas;
	}

	function crearAsignatura(){
		$sql="INSERT INTO asignaturas VALUES(NULL, '{$this->getNombre()}', {$this->getId_aula()}, {$this->getId_profesor()}, {$this->getId_grupo()})";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function tutoria(){
		$sql="INSERT INTO asignaturas VALUES (NULL, 'tutoria', {$this->getId_aula()}, {$this->getId_profesor()}, {$this->getId_grupo()})";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	function eliminarTutoria(){
		$sql="DELETE FROM asignaturas WHERE id_profesor={$this->id_profesor}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	function grupoAula(){
		$sql="SELECT id_grupo FROM asignaturas WHERE id_aula={$this->getId_aula()} AND nombre='tutoria'";
		$id_grupo= $this->db->query($sql);
		return $id_grupo;
	}

	function eliminar(){
		$sql="DELETE FROM asignaturas WHERE id_asignatura={$this->getId()}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

}

?>