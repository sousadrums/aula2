<?php

class Profesor{
	private $id_profesor;
	private $usuario;
	private $contraseña;
	private $nombre;
	private $apellidos;
	private $rol;
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}

	function getId_profesor(){
		return $this->id_profesor;
	}
	function getUsuario(){
		return $this->usuario;
	}
	function getContraseña(){
		return  password_hash($this->db->real_escape_string($this->contraseña), PASSWORD_BCRYPT, ['cost'=> 4]);
	}
	function getNombre(){
		return $this->nombre;
	}
	function getApellidos(){
		return $this->apellidos;
	}
	function getRol(){
		return $this->rol;
	}
	

	function setId_profesor($id_profesor){
		$this->id_profesor=$id_profesor;
	}
	function setUsuario($usuario){
		$this->usuario=$this->db->real_escape_string($usuario);
	}
		function setContraseña($contraseña){
		$this->contraseña= $contraseña;
	}
	function setNombre($nombre){
		$this->nombre=$this->db->real_escape_string($nombre);
	}
	function setApellidos($apellidos){
		$this->apellidos=$this->db->real_escape_string($apellidos);
	}
	function setRol($rol){
		$this->rol=$rol;
	}

	function getAlll(){
		$sql="SELECT * FROM profesores WHERE rol!='admin'";
		$profesores= $this->db->query($sql);
		return $profesores;

	}

	function getAll(){
		$sql="SELECT * FROM grupos g RIGHT JOIN profesores p ON g.id_tutor=p.id_profesor WHERE rol!='admin'";
		$profesores= $this->db->query($sql);
		return $profesores;
	}

	function getProfe(){
		$sql="SELECT * FROM profesores WHERE id_profesor={$this->id_profesor}";
		$profe= $this->db->query($sql);
		return $profe;
	
	}
	function getTutor(){
		$sql="SELECT p.id_profesor id_profesor, p.nombre nombre, p.apellidos apellidos, g.id_grupo id_grupo, g.nombreGrupo nombreGrupo, a.id_aula id_aula, al.codigo codigo FROM profesores p INNER JOIN grupos g ON p.id_profesor=g.id_tutor INNER JOIN asignaturas a ON a.id_profesor=p.id_profesor INNER JOIN aulas al ON al.id_aula=a.id_aula WHERE p.id_profesor={$this->id_profesor}  ";
		$tutor= $this->db->query($sql);
		return $tutor;
	}

	function getTutores(){
		$sql="SELECT p.id_profesor id_profesor, p.nombre nombre, p.apellidos apellidos, p.rol rol, g.nombreGrupo nombreGrupo FROM grupos g INNER JOIN profesores p ON g.id_tutor=p.id_profesor WHERE p.rol='tutor'ORDER BY p.id_profesor ASC ";
		$tutores= $this->db->query($sql);
		return $tutores;
	}
	


	public function save(){
		$sql = "INSERT INTO profesores VALUES(NULL, '{$this->getUsuario()}', '{$this->getContraseña()}', '{$this->getNombre()}', '{$this->getApellidos()}', 'profe')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function login(){
		$result = false;
		$usuario = $this->usuario;
		$contraseña = $this->contraseña;
		
		// Comprobar si existe el usuario
		$sql = "SELECT * FROM profesores WHERE usuario = '$usuario'";
		$login = $this->db->query($sql);
		

		if($login && $login->num_rows == 1){
			$profesor = $login->fetch_object();
			
			// Verificar la contraseña
			$verify = password_verify($contraseña, $profesor->contraseña);
			if($verify){
				$result = $profesor;
			}
		}

		return $result;
	}

	public function aTutor(){
		$sql="UPDATE profesores SET rol='tutor' WHERE id_profesor={$this->id_profesor}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}
	
	
	public function aProfe(){
		$sql="UPDATE profesores SET rol='profe' WHERE id_profesor={$this->id_profesor}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}

	public function edit(){
		$sql="UPDATE profesores SET usuario='{$this->getUsuario()}', nombre='{$this->getNombre()}', apellidos='{$this->getApellidos()}' WHERE id_profesor={$this->id_profesor}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function updateContraseña(){
		$sql="UPDATE profesores SET contraseña='{$this->getContraseña()}' WHERE id_profesor={$this->id_profesor}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}

		public function grupoSin(){
		$sql="SELECT * FROM grupos WHERE id_tutor=0";
		$gruposin= $this->db->query($sql);
		return $gruposin;
	}

	function aulasin(){
		$sql="SELECT a.codigo, a.id_aula FROM aulas a WHERE a.id_aula NOT IN (SELECT  a.id_aula  FROM aulas a RIGHT JOIN asignaturas s ON a.id_aula=s.id_aula)";
		$aulasin= $this->db->query($sql);
		return $aulasin;
	}


	
	public function quitarTutoria(){
		$sql="UPDATE grupos SET id_tutor=0 WHERE id_tutor={$this->id_profesor}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	public function borrarProfe(){
		$sql="DELETE FROM profesores WHERE id_profesor={$this->id_profesor}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}
}

?>