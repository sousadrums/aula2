<?php 


class Grupo{

	private $id_grupo;
	private $nombreGrupo;
	private $id_tutor;
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}


	function getId_grupo(){
		return $this->id_grupo;
	}
	function getnombreGrupo(){
		return $this->nombreGrupo;
	}

	function getId_tutor(){
		return $this->id_tutor;
	}


	function setId_grupo($id_grupo){
		$this->id_grupo=$id_grupo;
	}

	function setnombreGrupo($nombreGrupo){
		$this->nombreGrupo=$this->db->real_escape_string($nombreGrupo);
	}

	function setId_tutor($id_tutor){
		$this->id_tutor=$id_tutor;
	}



	function grupoDesdeId(){
		$sql="SELECT g.nombreGrupo FROM grupos g WHERE id={$this->getId_grupo()}";
		$grupo= $this->db->query($sql);
		return $grupo;
	}

	public function tutorGrupo(){
		$sql="UPDATE grupos SET id_tutor={$this->getId_tutor()} WHERE id_grupo='{$this->getId_grupo()}'";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	/*function tutoria(){
		$id_aula=$this->getId_aula();

		$sql="INSERT INTO asignaturas VALUES (NULL, 'tutoria', {$this->getId_aula()}, {$this->getId_tutor()}, {$this->getId_grupo()})";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
*/
	public function alumnosTutoria(){
		$id_grupo=$this->id_grupo;
		$id=$id_grupo->id_grupo;
		$sql="SELECT * ,(SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=1) AS amigos, (SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=2) AS enemigos, (SELECT p.coordx FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno AND p.guardado='sin') AS coordx, (SELECT p.coordy FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno AND p.guardado='sin') AS coordy FROM alumnos INNER JOIN grupos ON grupos.id_grupo=alumnos.id_grupo WHERE alumnos.id_grupo='{$id}'";
		$alumnos= $this->db->query($sql);

	return $alumnos;
	}
	public function grupoTutor(){
		$sql="SELECT id_grupo, nombreGrupo FROM grupos WHERE id_tutor={$this->id_tutor}";
		$grupo= $this->db->query($sql);

		return $grupo;
	}

	public function idTutorGrupo(){
		$sql="SELECT id_tutor FROM grupos WHERE id_grupo={$this->getId_grupo()}";
		$id_tutor= $this->db->query($sql);


		return $id_tutor;
	}

	public function grupos(){
		$sql="SELECT * FROM grupos  WHERE id_tutor=0 ORDER BY id_grupo ASC";
		$grupos= $this->db->query($sql);
		return $grupos;
	}
	public function aulaGrupo(){
		$sql="SELECT id_aula FROM asignaturas WHERE id_profesor={$this->getId_tutor()} ";
		$id_aula= $this->db->query($sql);
		return $id_aula;
	}

	public function grupoCon(){
		$sql="SELECT * FROM grupos g INNER JOIN profesores p ON  p.id_profesor=g.id_tutor WHERE p.rol!='admin' ORDER BY g.id_grupo ASC";
		$grupos= $this->db->query($sql);
		return $grupos;
	}
	

	function getAll(){
		$sql="SELECT * FROM profesores p RIGHT JOIN grupos g ON p.id=g.id_tutor WHERE rol!='admin'";
		$profesores= $this->db->query($sql);
		return $profesores;
	}

	public function save(){
		$sql = "INSERT INTO grupos VALUES(NULL, '{$this->getnombreGrupo()}', '0')";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function borrarGrupo(){
		$sql="DELETE FROM grupos WHERE id_grupo={$this->id_grupo}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	function alumnosGuardado(){
		$sql="SELECT DISTINCT p.guardado AS guardado FROM posiciones p INNER JOIN alumnos a ON a.id_alumno=p.id_alumno WHERE guardado!='sin' AND a.id_grupo={$this->getId_grupo()}";
		$guardados=$this->db->query($sql);
		return $guardados;
	}



} 
?>